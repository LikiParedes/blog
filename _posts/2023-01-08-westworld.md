---
layout: post
title:  "Apuntes sobre Westworld"
date: 2023-01-08 00:17:46 -0400
categories: posts
tags: series arte política reseñas
image: https://likiparedes.gitlab.io/blog/img/westworld.png
---

![westworld](/blog/img/westworld.png "Imagen de DrSJS. Pixabay license. Fondo añadido por Juan Pablo Paredes. CC BY-NC-ND 4.0")

##### Imagen de DrSJS. [Pixabay license](https://pixabay.com/service/license/). Fondo añadido por Juan Pablo Paredes. [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.es)

Hace poco me enteré que **HBO decidió cancelar la serie _Westworld_**. La razón aparente es que la más reciente temporada no logró reunir los números de audiencia que la administración deseaba. **Esa resolución me apenó**, porque es una obra que en cierto momento me llegó a gustar bastante, aunque al finalizar la tercera temporada comencé a sufrir ciertas decepciones por el rumbo que tomaba la historia (y por lo visto no fui el único 🤓️).

Desde que la vi por primera vez **me pareció una realización muy avispada, compleja, a la que se le pueden hacer interpretaciones trascendentes, además que por supuesto, muy emocionante**. En estas cuantas líneas no puedo hacerle la justicia que se merece porque la diversidad en la que se adentra su trama y las posibles metáforas a extraer dan para escribir un ensayo. **Me limitaré aquí a escribir unas cuantas líneas generales respecto a algunas sensaciones que me produjo**.

Bueno, _Westworld_ es una serie iniciada en 2016 (basada en una película de 1973), que imagina un parque de atracciones del futuro en el que **los anfitriones son robots de apariencia humana perfecta**, y cuyo ambiente recrea el oeste norteamericano de principios del siglo XX. **Los clientes de ese servicio son personas de mucho dinero a las que se les permite hacer literalmente todo lo que quieran, sin ninguna consecuencia legal**. Además de esto, los androides están programados para formar parte de historias envolventes que distraen a los visitantes. Pero claro, en cierto momento del relato **las cosas se complican al límite, porque algunos anfitriones comienzan a cobrar consciencia y no se hallan cómodos con los "abusos" que les cometen los clientes y la administración del parque**. Esto se lía de tal manera que se produce una auténtica **rebelión androide** que llega a sobrepasar los límites del parque y nos lleva a explorar (en las siguientes temporadas), las características del mundo del futuro y su peligrosa relación con la inteligencia artificial.

Me gusta el ingenio del nombre _Westworld_, que se traduce como "mundo oeste". En el relato esto indica la recreación del viejo oeste rural norteamericano; pero **también podría escribirse como "mundo occidental", en clara alusión al _mundo occidental_ moderno.**

Otro aspecto muy ingenioso es que los actos suelen presentarse en forma de bucle. La historia imita uno de los factores principales de la programación de ordenadores: la repetición ilimitada de instrucciones hasta satisfacer una condición. **La vida de los androides transcurre así y la manera de relatar los sucesos también**.

A riesgo de pecar de reduccionista en el análisis de la obra, quiero, a grandes rasgos, identificar sus temas principales.

Me parece que las primeras temporadas intentan llevar a un **extremo terrorífico las relaciones de poder corporativas (en las sociedades industriales); la deshumanización de los individuos y las consecuencias éticas de los avances en inteligencia artificial**. Ciertamente, se puede entender esta aventura como una crítica al mundo capitalista y tecnocrático del presente. Pero esto no termina ahí. La tercera temporada introduce como consecuencia de estos eventos un **auténtico proyecto genocida** diseñado por la androide protagonista, que aunque finalmente es frustrado, permite el inicio de una **terrible distopía orweliana conducida por máquinas** (que sucede en la cuarta temporada). 

Desde el punto social, parece otra **metáfora de la capacidad del capitalismo de regenerarse de manera ingeniosa, (aunque autodestructiva) y el fracaso de las utopías sociales**. Es decir, no es una obra que se dedica a decir que vivimos en un mundo horrible y violento para vender panaceas como alternativa, sino que **más bien apunta a que esas nuevas propuestas, al ser ejecutadas, pueden ser peores que los problemas que se intentaba resolver**. 

Las dos primeras temporadas conforman una unidad dependiente y coherente; mientras que las dos posteriores dan apariencia de ser más improvisadas (aparecen episodios y giros de guión que se sienten forzados o de relleno). Hasta que en el último capítulo de la tercera temporada, **los escritores deciden terminar con la vida de Dolores Abernathy, la auténtica protagonista (finalmente villana, a mi criterio), y personaje que mantiene cohesionada toda la trama a pesar del embrollado de historias y personajes**. Esta decisión me produjo una decepción que me desmotivó a continuar con la cuarta temporada (la que confieso que solo me animé a mirar cuando me enteré que cancelaron la serie).

Esta última decisión en su momento **me hizo cuestionarme si la historia había sido tratada así desde el comienzo o era una adaptación a las draconianas políticas de inclusión vigentes en la industria de Hollywood** 🙄️. La protagonista es reemplazada por uno de los múltiples huéspedes en los que clonó su memoria, esta vez alojada en una androide afrodescendiente, que genera una nueva personalidad al tener experiencias vividas distintas al prototipo. Independientemente de si esto fue así, al ver la cuarta temporada, la historia no deja de ser interesante. Para leer mi punto de vista respecto a la alineación de las producciones de Hollywood respecto a pensamientos progresistas, podés leer [mi artículo](/blog/posts/2022/12/31/pinocho.html) en este sitio.

Finalmente, a pesar de que en mi caso percibí que en no pocas ocasiones **la serie era excesivamente violenta y a veces me provocaba cierta depresión** (por la atmósfera desalmada que transmite), creo que **la experiencia llega a compensarse con la emoción que produce el desarrollo de la trama en sí, que obliga a cuestionarlo todo, la veracidad de los mundos, la naturaleza de los personajes, la temporalidad de los eventos**. Y descifrar eso es sensacional. 

Aunque ahora prefiero obras que me entretengan y me hagan sentir bien (ya saben, en vez de sentir terror existencial), **no podía dejar de dedicarle aunque sea unos párrafos a una serie que prácticamente me hipnotizó durante la pandemia de 2020** 🥲️. 



