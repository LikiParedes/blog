---
layout: post
title:  "Cómo NO hacer un blog"
date: 2022-11-13 20:18:59 -0400
categories: posts
tags: autobiografía tutoriales
image: https://likiparedes.gitlab.io/blog/img/blog.png
---

![blog](/blog/img/blog.png "Imagen de Mohamed Hassan. Pixabay license. Fondo añadido por Juan Pablo Paredes. CC BY-NC-ND 4.0")

##### Imagen de [Mohamed Hassan](https://www.instagram.com/mohamed_hassan515/). [Pixabay license](https://pixabay.com/service/license/). Fondo añadido por Juan Pablo Paredes. [CC BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.es)

Mi nombre es **Juan Pablo Paredes** y heme aquí presentando esta _bitácora personal en línea_ 🤵. 

El [protocolo](https://www.ecosia.org/search?q=como%20hacer%20un%20blog) establece que debería comenzar exponiendo con elocuencia el _tema del blog_; sus motivaciones; a quiénes está dirigido y así. **Pero anuncio que no podré complacer a cabalidad esa posibilidad**. Para definir eso, se supone que tendría que haber hecho una mínima indagación al mercado, estableciendo:

- Cuáles son los _temas de tendencia_,`googletrends`, para posicionar la página correctamente (ya saben, para no repetir y lograr algo de audiencia 🙄).
- Preguntarme cómo puede encajar este sitio en los términos de búsqueda web ¿Cuál es la probabilidad de que los buscadores lo coloquen en buenas posiciones,`keywords`, y alguien le dé click 🥲? (honestamente, creo que casi ninguna). 

En síntesis, **esforzarme para que las decisiones que tome valgan la pena,** para que el trabajo hecho no pase desapercibido, sea de utilidad a alguien y hasta tal vez lograr monetizarlo de alguna manera 🤑️.   

Si bien todas esas son, claramente, previsiones razonables, me parece que a fin de cuentas le quitan cierta diversión al asunto. **De tanto leer mercadeo, me pregunto en qué momento uno disfruta lo que hace**. Es que ¿hay que terminar escogiendo entre escribir lo que uno realmente _quiere_ o solo sondear lo que la gente en realidad _quiere_ leer? **Es terrible la encrucijada de quién quiere dedicarse a escribir algunas líneas** 😬️. 

Como verán a continuación, **no es que precisamente haya respondido a esta cuestión siguiendo el protocolo al pie de la letra** 😁. Creo que más bien la gente no sabe que necesita leer ciertas cosas.

## 1. De qué va esto

Antes de animarme a publicar la bitácora me dediqué a pensar bastante respecto a su posible tema principal para luego lanzarme a definir un _nombre llamativo_ y esas cosas. Sin embargo, fracasé en el intento. **Es que, no puedo concentrarme en un solo tema** 🤦. No es la primera vez que me pasa esto. Ya tuve blogs antes y en todos los casos terminó siendo un revoltijo de asuntos. No se si sirva de mucho, pero esta vez intenté hacer algo distinto 🙋. **En vez de buscarle un nombre ingenioso que intente representar una diversidad de asuntos, le puse simplemente mi nombre**. No es que esté buscando consolidar una _marca personal_ o algo así, pero creo que a fin de cuentas si aquí escribiré respecto a lo que me gusta, qué mejor identificación para esta curiosa recopilación de contenido que mi propio nombre 🙂.

## 2. Quién soy

Para establecerlo brevemente, **actualmente soy _profesor de Geografía_ en secundaria**. Vivo en La Guardia, departamento de Santa Cruz, Bolivia. **Me gusta mucho mi trabajo**; pero no siempre me dediqué a ser profesor. 

Anteriormente manejé un archivo histórico durante años; previo a eso trabajaba con grupos sociales que querían organizarse para mejorar su situación socioeconómica (y al mismo tiempo aportar al cuidado el medio ambiente); viajé mucho por las zonas rurales de la región. En fin. **Tengo estudios universitarios en _Sociología_ e _Historia_**. Y para agregarle un poco más de peculiaridad a aquello, hace algunos años, así sin más, **empecé a sentirme fuertemente atraído hacia la informática y para ser más específico, hacia el _software libre/código abierto_ y el sistema operativo** `linux`. Y si bien no tengo estudios profesionales en ello (ganas no me faltaron), [encuentro la necesidad de mencionarlo aquí](/blog/posts/2023/01/16/linux.html) porque actualmente no solo gasto mucho de mi tiempo consumiendo información sobre software y hardware, sino que es muy probable que gran parte de lo que aquí escriba sea sobre eso.

## 3. Entonces al fin y al cabo qué se abordará aquí

Bueno, esa es la cuestión. **Después de pensar un poco creo que simplemente no podré evitar escribir lo que me viene en gana** (perdón por lo chabacano 🤭). Ya saben, cuando más bien debería estar buscando "montarme en la ola" de las tendencias 🏄.

En sí, los temas que me interesan podrían agruparse en: 
- **Linux** (software libre, código abierto). 
- También sobre **educación** (recordemos que soy profesor). 
- **Política.** 
- **Ciencias sociales.** 
- **Historia**, **cine** y a veces el **arte**. 
- Y les tengo noticias, no es una lista cerrada 😅.

Si tendría que explicar cómo se vinculan estos temas entre sí para mí o por qué son los que más me interesan o incluso jerarquizarlos de alguna manera de acuerdo a mi predilección, **podría mencionar que el primer tema que me atrajo desde joven fue la Filosofía y las Ciencias sociales en específico** y aunque suene raro, creo que a partir de allí comencé a ramificarme hacia el resto. Luego, de alguna forma cada uno de los demás temas comenzó a adquirir "autonomía", es decir, ya no los vinculaba necesariamente con un punto de vista social.

Tal vez alguien aquí se pregunte (y con razón), cómo diablos se puede derivar el tema informático de un estudio social. Aunque no lo crean, si indagan un poco en los postulados del software libre y de las distribuciones _GNU/Linux_ (lean los [manifiestos](https://www.debian.org/social_contract.es.html) de los proyectos sobretodo), se darán cuenta que sí, **la informática con una motivación socialmente intencionada sí existe**. Cuéntenle eso a su psicólogo 😁.

## 4. – 😒 ya, ya ya. Y a quiénes va dirigido esto.

Y sí. **No creé este blog pensando en satisfacer la curiosidad de un público específico**, es decir, segmentado previamente en base a pormenorizados estudios de mercado. Estuve indagando un poco en esto de las tendencias y no encuentro un sitio que englobe estos temas simultáneamente. **O sea, de entrada no encajo** 🦹. 

- Los canales de **Linux-software libre** suelen a veces tocar la justificación e **impacto social** de los proyectos o relacionarlos con la **educación**. 
- Por otro lado hay páginas que en artículos dispersos relacionan la **educación** con la **política** o la **sociedad**. 
- Finalmente, como las **Ciencias sociales** tienen una vocación, casi diríamos enciclopédica, en torno al actuar humano en grupo, es probable encontrar también de forma aislada páginas que abordan cualquiera de los anteriores temas mencionados desde el punto de vista social.

Entonces, avanzando hacia el tema de quiénes podrían ser los potenciales lectores de este blog simplemente puedo atinar a decir que, podrían ser personas que se acercan a cada uno de estos temas de forma fragmentada (Ciencias sociales, educación, sistemas operativos, qué se yo). **O en realidad, probablemente nadie** 🙀 

## 5. Finalmente...

Nada. 

Como insinuaba en mi disparatada presentación, **pienso que un blog requiere motivación, pasión**. En mi caso siento necesidad de comunicarme de esa manera. **Pero creo que también demostré que eso no es suficiente.** 

**En el otro extremo, se puede comparar un blog con una empresa**. Hay que estudiar el mercado porque al fin y al cabo el mundo funciona así. La bitácora viene a ser un producto que será consumido y si no se tiene claro a quién va dirigido y cómo convencer a esas personas que le presten un poco de atención, probablemente nadie lo "comprará". Sin embargo, como acabo de demostrar, **no estoy preparado para ese tipo de objetivos**. Es decir, no quiero llegar al extremo de hacer **lo que sea** por lograr un espacio en línea.

Al final, no es difícil darse cuenta que esto se puede solucionar buscando cierto equilibrio. Así que lo que ofrezco es lo siguiente. **En este espacio voy a vaciar mi interior tal cual** (que para mí es lo más importante), **pero voy a hacer un _pequeño esfuerzo_ por hacerlo asimilable 🤏 y encajar en la actualidad** 🤡. Si lo que propongo no es suficiente para la mayoría puedo vivir con eso. No soy budista pero tampoco me obnubila la atención. Quiero mantenerme feliz y si dos personas comparten algo conmigo para mi es suficiente 😺.

