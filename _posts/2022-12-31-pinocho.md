---
layout: post
title:  "Pinocho contra el fascismo"
date: 2022-12-31 00:18:59 -0400
categories: posts
tags: películas política arte reseñas
image: https://likiparedes.gitlab.io/blog/img/pinocchio.jpg
---

![pinochio](/blog/img/pinocchio.jpg "Foto de Jörg Peter. Pixabay license")

##### Foto de Jörg Peter. [Pixabay license](https://pixabay.com/service/license/)

Acabo de mirar el largometraje de animación en volumen, _Pinocho_, de Guillermo del Toro (ya saben, el viejo cuento de la figura de madera que cobra consciencia y cuyo principal deseo es convertirse en un niño de verdad). Me decidí a ver esa película luego de leer algunos artículos (y varios estados de Twitter), que **la recomendaban y promovían como una obra de arte cautivadora**. Pero, ¿qué creen? **cuando terminó, no me impresionó demasiado y por si fuera poco, en realidad, me causó cierta preocupación, por su trama propagandista** 📢️.

No voy a dedicarme en este escrito a valorar demasiado los aportes artísticos de la obra. Si bien, me encanta el cine, (consumo filmes y series de manera bastante porfiada desde mi adolescencia), no es mi área profesional 😁️. **Para lo que si estoy entrenado, sin embargo, y de manera competente, es para plantear conjeturas respecto a las motivaciones de los agentes sociales**, en este caso, **los productores de la película** ☝️🙂️. 

– ¿Y qué diablos es eso? 😕️ 

Veamos a continuación. Empecemos mencionando que el filme evidentemente tiene aspectos novedosos. Hay un logro respecto a la animación fotograma a fotograma, la belleza de las figuras, la composición y demás. La historia ata mejor los cabos y da mayor contexto a las acciones de los personajes que versiones pasadas, incluso llegando a situar la atmósfera, tanto histórica como políticamente (**aunque claro, esto último para mí no es gratuito**). En ocasiones la secuencia consigue generar emociones intensas, pero en mi experiencia esto no fue constante. 

A pesar de estas consideraciones muy personales, a diferencia de la crítica, como mencioné, **no percibo que la película tenga aportes demasiado transformadores respecto a la ya trillada historia**. Lo que me permite llegar al tema central de este post. **Creo más bien que el trabajo destaca solo por formar parte de un movimiento cultural contemporáneo con clara intencionalidad política y cuyas producciones a decir verdad ya comienzan a aburrirme**. 

– 😒️ ¿En serio?

Sí, pero no me malinterpreten. Antes quiero aclarar que no hallo que esto sea algo malo en sí. Creo que los productores cinematográficos **tienen todo el derecho de utilizar los medios que tienen a su disposición para generar arte que al mismo tiempo desee transmitir un mensaje social** (instrumentalizado por la política, claro está). Sin embargo, ante esa circunstancia, **yo como espectador asumo como mi responsabilidad el hacer notar el contenido de ese mensaje y ahondar en las consecuencias que dicha posición provoca en el mundo que vivimos**. 

## 1. No es una excepción

Comencemos comparando un aspecto de esta película con otros filmes de Guillermo del Toro. Obras en las que funje tanto como director, guionista y productor, es decir, **que son películas muy suyas, muy íntimas** 🤩️📽️. 

Pregunto, **¿qué tienen en común _El laberinto de fauno_, _La forma del agua_ y _Pinocho_?**

– 🤔️ 

**Si pensanste que en las tres películas los antagonistas son fascistas, acertaste** 😁️👍️. 

– ¿El coronel Richard Strickland de _La forma del agua_ es un fascista? 😳️

Bueno, es necesario matizar un poco (anuncio de un largo y aburrido paréntesis 🙂️).

– 🙄️

En _La forma del agua_, el némesis es un coronel norteamericano a cargo de un experimento militar en plena _Guerra fría_, que si bien no forma parte de un gobierno que asume una ideología fascista como tal (la democracia norteamericana de los años 60), **para los efectos de la trama, el comportamiento del personaje** (un frenético criminal en uniforme que defiende a través de todos los medios la seguridad de una potencia), **se acerca a una acepción secundaria del [concepto](https://dle.rae.es/fascismo)**. 

Además que, bueno, actualmente existe una vulgarización peligrosa de la palabra (ya saben, fascismo es una ideología europea concreta de principios del siglo XX). Se suele adjudicar injustamente ese mote a cualquier individuo que muestra cierta energía en propagar ideales no progresistas. **Aunque no me parece que aplicarle ese término a este personaje sea históricamente correcto, creo que acomodárselo en este escrito ayuda a ilustrar lo que sostengo a continuación, porque percibo que es parte de la visión del director que estamos analizando**.

– 😵‍💫️

Bueno, prosigamos.

**Respecto a los villanos de _El laberinto de fauno_ y _Pinocho_ no hay discusión**, son fascistas tanto en su militancia política (histórica), como en su comportamiento. En el caso de Pinocho incluso aparece el mismísimo Benito Mussolini en persona. 

Ahora, ¿a qué viene ésto?, **¿cuál es el problema con que Guillermo del Toro estimule constantemente su imaginación matando fascistas?**, ¿acaso no son una terrible calamidad? 

**Matar en defensa propia a un fascista encolerizado es todavía un buen (y trillado) final para obras de todo tipo en nuestra civilización contemporánea**. No tengo ningún problema con eso.

Lo que me llama la atención es la frecuencia de este tipo de escenas en las obras de este director y similares en otros. **Me da la impresión de que esto ya raya en el adoctrinamiento**. Me pregunto, ¿porque últimamente el enemigo debe ser siempre un fascista? **Si hay necesidad de que el adversario sea político ¿por qué no de vez en cuando escoger a un comunista?**, por ejemplo. Digo, porque demostraron ser igual de sociópatas ¿verdad?. ¿Por qué no plantear una convicción más pluralista? Es decir, **para mandar el mensaje de que imponer las ideas por la fuerza bruta (fascismo), sean cuales sean, no se justifica**. 

Y es que aquí está la cuestión. **Creo que esta gente no apunta precisamente a una búsqueda de "consenso" o "moderación" en sus mensajes**. Creen que hay ciertas ideas, digamos una "justicia social", que sí pueden llegar a acreditar cierta violencia. **Y lo que sale de aquello, bueno, eso es "fascismo"** 🙂️. Aunque suene a una acusación exagerada, en esa lógica maniquea e hipócrita vive cierto sector del mundo actualmente ☯️.

Pero, incluso vayamos más allá. Creo que si Guillermo propone a un antagonista comunista en sus películas **corre el riesgo de ser brutalmente cancelado por el progresismo actual**, que no es poca gente. Es más, probablemente él mismo no sea de la idea de hacer eso por convicción. 

Entonces ¿qué?, ¿estoy acusando a Guillermo del Toro de ser un comunista clandestino (al estilo J. Edgar Hoover de los 60)? 🤨️🔎️ 

No necesariamente.

**Lo que quiero demostrar es que su "arte" está tomando partido en un panorama irracional de polarización política contemporánea**.

– 😳️ ¿qué?

## 2. Política y cine

No es algo nuevo. **Desde siempre la política se metió en el cine**, unas veces de maneras sutiles y otras no tanto: 

- En los 40 del siglo XX vemos una película del Pato Donald en la que el personaje **parodia al régimen nazi de la época**, por ejemplo. Igual que ahí, pregunto **¿había necesidad de que tanto el Pato Donald como Pinocho se enfrenten a los fascistas?**
- Durante la **Guerra Fría** se presenta _Rocky IV_, película en la que el boxeador estadounidense pelea con un despiadado y poderoso competidor de la URRSS, **en una referencia simbólica a la lucha del capitalismo contra el socialismo**. 

Luego de la caída del muro de Berlín, me da la impresión de que **el cine se enriqueció de diversidad**, no solo en géneros sino también en ideología:

- Había esas películas en las que **el nacionalismo norteamericano se promocionaba de manera algo torpe**, como en _Avión presidencial_ de Wolfgang Petersen e **incluso a escala galáctica** con _Día de la independencia_ de Roland Emmerich. 
- Pero también se popularizaron otras que exactamente hacían lo opuesto, es decir, **situaban al gobierno norteamericano como villano**. Se me vienen a la mente _JFK_ y _Nacido el 4 de julio_, ambas de Oliver Stone. 
- En una **línea menos dura (gobierno negligente o torpe)** _The Terminator_ de James Cameron o _La Roca_ de Michael Bay son buenos ejemplos. 

Y en este contexto nos acercamos al tema en cuestión. En esta década surgen con fuerza películas que a la par de esto también visibilizaban **luchas de colectivos vulnerables, como los casos de identidad y orientación sexual diferentes**, en _Philadelphia_ de  Jonathan Demme, **o los afrodescendientes**, en _Tiempo para matar_ de Joel Schumacher entre otros.  

– ¿Y? 🤨️

Y aquí es donde me parece que empieza **el embrollo actual**. Voy a detenerme un poco en la política.

Ciertamente, el ataque al World Trade Center de Nueva York en 2001 y las posteriores guerras punitivas en las que ingresó Estados Unidos **modificaron sus relaciones de poder internas de un modo terrible**. Los **sectores conservadores se radicalizaron de formas preocupantes**. Se incrementa la fobia y violencia hacia los inmigrantes, identidades de género no convencionales e ideologías políticas moderadas y progresistas. Sin embargo, **del otro lado surge extremismo también y esa es la situación que quiero visibilizar aquí**. Se comienza a villanizar lo tradicional y se exagera la victimización de sectores diversos con ambiciones políticas y tácticas de confrontación inescrupulosas. ¿A qué me refiero? Quien no se identifica con criterios de "minorías" culturales, es tachado de racista; quien no apoya iniciativas de identidades de género y orientaciones sexuales no convencionales, es tachado de homofóbico, transfóbico y otros; y finalmente, **quien no simpatiza con una ideología política progresista, es un fascista.** ¿Ahora entienden? 😉️

– 🥱️ no

**Ya no es necesario discriminar para ser acusado de discriminación y se está neutralizando y hasta estigmatizando a una gran proporción de población con etiquetas maliciosas**.

Déjenme reforzar mi idea con más ejemplos. **Creo que Hollywood actualmente está demasiado alineado hacia ese progresismo radical**. Y no es que tampoco esté abogando por las ideas del espectro conservador. Es solo que como un usuario acostumbrado al cine de los 90's en el que había variedad, esto ya me parece irritante. **Uno no sabe si está disfrutando de entretenimiento o está recibiendo panfletos ideológicos incluso en películas para niños**. Vamos al tema:

* En 2019 miré la serie de HBO _Whatchmen_ de Damon Lindelof y lo admito, en ese momento me encantó, no me percataba de asuntos políticos forzados y esas cosas. En muchos casos los guionistas logran hacer una auténtica obra de arte a pesar de aquello y sigo pensando eso de esta serie. Sin embargo, luego me enteré que Alan Moore, el autor verdadero de la historia, es decir, el de los cómics en los que está basada la serie, [la despreciaba](https://www.tomatazos.com/noticias/780891/Watchmen-Alan-Moore-pidio-que-no-lo-involucraran-en-la-vergonzosa-adaptacion-de-HBO) porque entre otras cosas **desvirtuaron el mensaje que quiso dar inicialmente, con panfletos de moda, en este caso, contra el supremasismo blanco entre otras cosas y las reivindicaciones afrodescendientes**.

* Recientemente acabó la serie de _Los anillos de poder_ de J.D. Payne y Patrick McKay, y es horrible. Pero no creo que esto se deba tanto a las "inclusiones forzadas" que se hacen en la trama. Pienso que simplemente el guion es pésimo. Las críticas anteriormente mencionadas se refieren a la presentación de personajes con características afrodescendientes; monopolización de roles superlativos en mujeres y colectivos considerados como "desfavorecidos"; estupidez y perversidad masculina y anglosajona. Y todo esto a expensas de la historia original, es decir, situaciones no basadas en lo que estableció J.R.R Tolkien en su obra de fantasía medieval. Lo que me parece cuestionable no es tanto esto, (aunque creo que es importante conservar fidelidad a las historias), sino que **la producción se puso en plan de que todo aquel que cuestionara la calidad de esta serie era un racista, homofóbico, misógino y todos esos adjetivos de moda que utiliza para defenderse uno de los bandos de la polarización política actual**.

* Finalmente, _Merlina_ de Tim Burton me pareció entretenida. Sin embargo, el hecho de que el villano sea el cadáver de un colono británico, que revive gracias a los conjuros de un culto supremasista contemporáneo de características caucásicas, y que desprecia a los seres diferentes (entiéndase todo lo no tradicional anglosajón), ya me pareció demasiado. 

**¿Es que no hay otro tema qué tocar que la lucha contra el "heteropatriarcado" blanco?** No hace falta aclarar que este no es el enfoque que tenía la historia original de los _Locos Addams_.

– ¿Entonces?

## 3. Basta de regaños hipócritas

> Tenemos arte para no morir de la verdad (Frierich Nietzsche)

Esto va más allá de que un aficionado al cine nostálgico como yo se sienta incómodo por la falta de variedad de ideas en la cinematografía actual. **Creo que esta terapia de superioridad moral transmitida a través de los medios de difusión contemporáneos hace más daño del que muchos pueden percibir**.

Hace ya bastante tiempo que las grandes producciones de Hollywood se hallan alineadas al progresismo y esto no es paranoia. Gran parte de las élites norteamericanas ven que promover ideas de inclusión es beneficioso a sus intereses, para lograr una cohesión social en disparidad, al parecer. Pero se les va la mano en cuanto a combatividad. Esto llegó a tal punto que **la Academia de los Óscares estableció que solo admitiría en sus evaluaciones películas que cumplan con [cuotas de representación](https://cnnespanol.cnn.com/2020/09/09/los-oscar-imponen-requisito-de-inclusion-para-aspirar-al-premio-a-la-mejor-pelicula/) de grupos considerados anteriormente "subrrepresentados"**. Esto es algo así como la regulación política del arte 😳️. 

Las consecuencias son producciones como _Los anillos de poder_ (ya mencionados), donde se presentan personajes con características culturales o de género, por ejemplo, que no corresponden a la historia de origen y esto desemboca en tramas como la de _Merlina_, en la que posiblemente es inevitable caer en el mismo patrón que otras: "la lucha contra el 'heteropatriarcado' blanco" 🙄️.

Desde el punto de vista político esto puede parecer astuto, tomando en cuenta la pluralidad poblacional del norte, pero en un ambiente de división radical de la sociedad más bien puede ser contraproducente. 

Creo que hay un mal diagnóstico de parte de estos movimientos. **Probablemente creen que las campañas de sensibilización respecto a la tolerancia a la diversidad al estilo de las películas de los 90 no eran suficientemente efectivas y optaron por algo más agresivo, imponer su visión y acusar de discriminación a todo aquel que no esté de acuerdo con ellos**. Sin embargo, **esto está provocando en muchas ocasiones el efecto contrario**, es decir, que pierdan apoyo del resto de la sociedad. 

Ejemplos. La victoria electoral del candidato conservador Donald Trump en las elecciones presidenciales del 2016. Hay quienes interpretan que las recientes pérdidas económicas en [Disney](https://youtu.be/OmxzeIZFfnU) y [Netflix](https://twitter.com/elonmusk/status/1516600269899026432) son producto de este proselitismo [woke](https://es.wikipedia.org/wiki/Woke) en el que claramente están inmersos. Y lo que es peor, **están provocando que grupos conservadores suenen sensatos en sus [observaciones](https://www.youtube.com/watch?v=Y8ohnVkizvk) a estas manifestaciones**. 

Finalmente, lo mas preocupante para mí es que **el _liberalismo norteamericano_ se tornó irónicamente demasiado agresivo**.

Veamos un ejemplo dentro de Hollywood. Este año estuvo a punto de ganar el Oscar a mejor película _El poder del perro_ de Jane Campion. Y a pesar de que concuerdo en que es una película muy bien trabajada técnicamente, muy emocionante quizá, su mensaje es atroz. El héroe del largometraje es un adolescente homosexual que seduce al cuñado de su madre (quién la atormentaba psicológicamente con su personalidad de macho alfa). El cuñado es un vaquero de Montana de principios del siglo XX (claro, aparentemente heterosexual), y luego de ganarse su cariño y confianza lo envenena. ¿El motivo? Lograr que su madre viva su vida tranquila al lado de su nuevo esposo y disfrute de opulencia con él. **¿Mensaje? La literal aniquilación del "heteropatriarcado" hipócrita, poderoso y "misógino" por parte de los que se sienten diferentes, está plenamente justificada**.

Y algo más grave. **Este _liberalismo norteamericano_ tiende a encontrar puntos de encuentro con el _progresismo totalitario latinoamericano_**. Los nexos a veces no son tan ambiguos y disimulados (vean este curioso [proselitismo](https://www.youtube.com/watch?v=FsZ3p9gOkpY) que hace el presentador y comediante británico radicado en Estados Unidos, John Oliver, a favor del candidato del Foro de Sao Paulo en las elecciones presidenciales de Brasil en 2018).  **Algo muy similar a lo que sucedió entre los conservadores estadounidenses y las dictaduras militares en los años 70. Es decir, del otro lado del espectro**. Pero, claro, el fascismo es muy malo ¿verdad? 

He ahí la explicación de por qué a Guillermo del Toro no le entretiene tanto poner villanos comunistas en sus obras 🙂️. **¿Más producciones de este tipo? No, por favor.**
