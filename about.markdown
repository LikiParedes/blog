---
layout: default
title: Acerca de
permalink: /about/
---
![about](/blog/img/about.jpg "Una de mis más grandes aficiones es el uso y configuración de Linux, Debian GNU/Linux para ser concreto. Ilustré con IA el sentimiento de estabilidad, seguridad y creatividad que me proporciona este sistema operativo, en un entorno real usualmente destructivo y caótico")

##### Una de mis más grandes aficiones es el uso y configuración del sistema operativo Linux, en su versión Debian GNU/Linux. Ilustré con IA el sentimiento de estabilidad, seguridad y creatividad que me produce este software, en un entorno real usualmente destructivo y caótico

Estimados lectores, bienvenidos a mi _blog_. Para ustedes que vinieron a husmear en este sector, tal vez con la expectativa de encontrar la indicación de un propósito, permítanme informarles que por el momento no hay mucho más por manifestar que lo ya mencionado en otros pasajes de la página 🤷️. **Es un proyecto nuevo y se me ocurre que a medida que avance podría llegar a consolidar una identidad más trascendente** 🧑‍🔧️. Aunque si al final todo se mantiene como ahora, así sencillo, tampoco lo vería como un fracaso. Considero este inicio satisfactorio y bien podría tener estas características hasta el final 👋️.

En la [primera publicación](/blog/posts/2022/11/14/como-no-blog.html) se puede hallar una pequeña sintésis del proceso de creación de la página y sus motivaciones. Allí dejo en claro que **uso esta herramienta simple y llanamente para mi satisfacción personal, para volcar mis percepciones sobre temas que me interesan**. Subordinado a aquello están otros requerimientos, como el de "adaptar" esta obra al mercado y esas cosas. 

A medida que logre más publicaciones podré engrosar este prólogo, aclarar más respecto a mis motivaciones y percepciones generales e incluso tal vez llegar a modificar la visión del sitio en sí 😅️.

###### © Juan Pablo Paredes Daza, 2022 - 2023. 

###### Salvo que se indique lo contrario, todo el *contenido publicado* en el blog se encuentra bajo los términos de la **Licencia Creative Commons Atribución-NoComercial-SinDerivadas 4.0 Internacional**.

###### Consulte la página de [Creative Commons](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.es) para más detalles.
