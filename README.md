## Bitácora personal de Juan Pablo Paredes.

© Juan Pablo Paredes Daza, 2022. 

El *código fuente* del blog se basa en el de [jekyll](https://gitlab.com/pages/jekyll). Se hicieron algunas modificaciones al *código*. Se creó un tema "oscuro" basado en el [tema oficial de jekyll](https://gitlab.com/jekyll-themes/default-bundler) de tonos claros. Del mismo modo se añadió algunas funcionalidades que el tema por defecto de jekyll no tenía, como una página de agrupación de post por etiquetas y escritura automática de las mismas en los metadatos.

El *código fuente* se resguarda bajo lo estipulado por **The MIT License**. Del mismo modo que el proyecto jekyll original.

> Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

**Este archivo forma parte del código fuente del blog de Juan Pablo Paredes**.

Salvo que se indique lo contrario, todo el *contenido publicado* en el blog se encuentra bajo los términos de la **Licencia Creative Commons Atribución-NoComercial-SinDerivadas 4.0 Internacional**.

Consulte la página de [Creative Commons](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.es) para más detalles.

